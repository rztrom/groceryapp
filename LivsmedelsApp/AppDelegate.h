//
//  AppDelegate.h
//  LivsmedelsApp
//
//  Created by Anders Zetterström on 2015-02-19.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

