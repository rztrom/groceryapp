//
//  ViewController.m
//  LivsmedelsApp
//
//  Created by Anders Zetterström on 2015-02-19.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

#import "ViewController.h"
#import "TableViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (weak, nonatomic) IBOutlet UITextView *resultText;
@property (weak, nonatomic) IBOutlet UILabel *noResultTextLabel;
@property (nonatomic) NSString *searchFieldTextTemp;
@end

@implementation ViewController

- (IBAction)onSearch:(id)sender {
    //skapa upp requesten och lägger till det vi söker efter
    NSString *searchString =[NSString stringWithFormat:@"http://matapi.se/foodstuff?query=%@", self.searchField.text];
    // ändra sökningen så att det går att söka med svenska bokstäver
    searchString = [searchString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //lägger till adressen/sökningen i en url
    NSURL *url = [NSURL URLWithString:searchString];
    //skicka in url i ett request objekt
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    //skapa en session
    NSURLSession *session = [NSURLSession sharedSession];
    // skapa datatask
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        ///kollar om vi fått in fel (om nätverk eller server är fel)
        NSLog(@"completed! Data: %@, Error: %@", data, error);
        
        // sätter sökordet till en annan sträng så det går att radera sökfältet
        self.searchFieldTextTemp= self.searchField.text;
        
        if(response){
            NSLog(@"response");
            dispatch_async(dispatch_get_main_queue(),^{
                //sätter texten för misslyckad sökning
                self.noResultTextLabel.text=[NSString stringWithFormat:@"Inget resultat på: %@",self.searchField.text];
                // visar texten för misslyckad sökning
                self.noResultTextLabel.hidden=NO;
                // gör sökrutan tom
                self.searchField.text=@"";
            });
        }
        // skapar upp parsingError (sätts
        NSError *parsingError= nil;
        //7' logga error
        if(error){
            NSLog(@"Error in response :%@",  error);
            return ;
        }
        // ta emot data i en array
        NSArray *tempArray= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parsingError];
        // om laddningen av data gick ok
        if(!parsingError){
            //sätter min array med hämtad data
        self.fullList =tempArray;
            //går till main queue
           dispatch_async(dispatch_get_main_queue(),^{
               // om arrayen inte är tom så körs seguen
                if(self.fullList.count>0){
                    [self performSegueWithIdentifier:@"showTableSegue" sender:self];
                }else {
                    self.resultText.text = @"no topics found";
                }
            });
        } else {
            // om det blev parsing error , skriver ut i loggen
            NSLog(@"Couldnt parse json : %@", parsingError);
        }
    }];
    // börjar köra uppgiften
    [task resume];
    // döljer tangentbordet
    [self.view endEditing:YES];
    self.noResultTextLabel.hidden=YES;
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    TableViewController *tvc = [segue destinationViewController];
    // sätter titeln till sökordet
    tvc.title = [NSString stringWithFormat:@"Sökning för: %@",self.searchFieldTextTemp];
    // sätter sökresultatet till arrrayen i tabelviewcontrollern
    tvc.fullList=self.fullList;
}

-(void)viewWillAppear:(BOOL)animated{
    //döljer texten som visas om ingen sökning hittas
    self.noResultTextLabel.hidden=YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //sätter text på titeln
    self.title =@"MATapi";
    // sätter färgen på titeltexten
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : self.view.tintColor}];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
                                  
                                  
@end