//
//  SearchCell.h
//  LivsmedelsApp
//
//  Created by Anders Zetterström on 2015-03-03.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cellIcon;
@property (weak, nonatomic) IBOutlet UILabel *itemCellLabel;

@property NSNumber *number;

@end
