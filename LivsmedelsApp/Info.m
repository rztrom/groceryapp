//
//  Info.m
//  LivsmedelsApp
//
//  Created by Anders Zetterström on 2015-02-19.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

#import "Info.h"

@interface Info ()
@property (nonatomic) NSTimer *timer;
@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UICollisionBehavior *collision;
@property (weak, nonatomic) IBOutlet UILabel *healthyTextLabel;
@property(nonatomic) NSString *carbonhydrate;
@end

@implementation Info

-(NSString*) cachePath{
    // skapar upp en sökväg
    NSArray *dirs = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // använder katalogen i första platsen i arrayen
    NSString *documentDirectory = dirs[0];
    // skapar hela sökvägen för bilden som ska hämtas
    NSString *completePath = [documentDirectory stringByAppendingPathComponent:self.itemNumber];
    return completePath;
}

-(void) saveImageToCache:(UIImage*) image{
    // gör om bilden till data
    NSData *data = UIImagePNGRepresentation(self.itemImage.image);
    //sparar ner bilden till sökvägen i cachePath och retunerar BOOL
    BOOL success= [data writeToFile:[self cachePath] atomically:YES];
    if(!success){
        NSLog(@"Failed to save image to cache");
    }
}

- (IBAction)onTakePhoto:(id)sender {
    UIImagePickerController *picker =[[UIImagePickerController alloc] init];
    picker.delegate = self;
    // tillåter editing (crop)
    picker.allowsEditing = YES;
    // väljer källa till fotoalbumen
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    // förflyttar oss till fotoalbumen
    [self presentViewController: picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    // tar bort kamerarullen
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    // skapar upp en bild med hjälp av den editerade bilden
    UIImage *image = info[UIImagePickerControllerEditedImage];
    //sätter bilden till den sparade bilden
    self.itemImage.image = image;
    // skickar in bilden till metoden som sparar bilden
    [self saveImageToCache:image];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    // tar bort kamerarullen
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)viewDidAppear:(BOOL)animated{
    // sätter nyttighetssträngen
    self.healthyTextLabel.text= [NSString stringWithFormat:@"Nyttighet: %d/5",self.numberOfStars];
    // skapar upp en bild och gör den till bilden i sökvägen för cachePath
    UIImage *image = [UIImage imageWithContentsOfFile:self.cachePath];
    // kollar så att det är en bild
    if(image){
        //sätter bilden
        self.itemImage.image = image;
        
    }else {
        NSLog(@"Failed to fetch image %@ from file system", self.cachePath);
    }
    
}

-(void) dropStars{
    
    self.animator =[[UIDynamicAnimator alloc]initWithReferenceView:self.view];
    self.gravity= [[UIGravityBehavior alloc] initWithItems:@[]];
    // lägger till beteende till animator
    [self.animator addBehavior:self.gravity];
    
    // sätter fast bilden med snap
    UISnapBehavior *snap = [[UISnapBehavior alloc] initWithItem:self.itemImage snapToPoint:self.itemImage.center];
    // hur mycket bilden skakar vid kollision
    snap.damping = 0.01f;
    // lägger till beteendet till animator
    [self.animator addBehavior: snap];
    
    //loopar igenom och skapar upp stjärnorna
    for(int i=0; i<self.numberOfStars;i++ ){
        //skapar upp en bild med pos och size
        UIImageView *star = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.center.x + 30 * i, 0 + 30 * i, 25, 25)];
        //sätter bild till vyn
        star.image = [UIImage imageNamed:@"star.png"];
        // lägger till vyn
        [self.view addSubview: star];
        
        // bestämmer kollisionen
        UICollisionBehavior *collision = [[UICollisionBehavior alloc] initWithItems:@[star, self.itemImage]];
        collision.translatesReferenceBoundsIntoBoundary=YES;
        //lägger till beteende
        [self.animator addBehavior:collision];
        //lägger till gravitation på stjärnan
        [self.gravity addItem:star];
    }
}

- (void)viewDidLoad{
    [super viewDidLoad];
    // gör labels synliga
    self.itemTextLabel.hidden=NO;
    self.energyTextLabel.hidden=NO;
    self.proteinTextLabel.hidden=NO;
    self.fatTextLabel.hidden=NO;
    self.carbohydratesTextLabel.hidden=NO;
    // startar animation
    [UIView animateWithDuration:1.5 animations:^{
        //förflyttning av labels
        self.itemTextLabel.center = CGPointMake(self.itemTextLabel.center.x, self.itemTextLabel.center.y+30);
        self.energyTextLabel.center = CGPointMake(self.energyTextLabel.center.x-200, self.energyTextLabel.center.y);
        self.proteinTextLabel.center = CGPointMake(self.proteinTextLabel.center.x-200, self.proteinTextLabel.center.y);
        self.fatTextLabel.center = CGPointMake(self.fatTextLabel.center.x-200, self.fatTextLabel.center.y);
        self.carbohydratesTextLabel.center = CGPointMake(self.carbohydratesTextLabel.center.x-200, self.carbohydratesTextLabel.center.y);
    } completion:nil];
    
     // fördröjer släpp av stjärnor
    self.timer= [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(dropStars) userInfo:nil repeats:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
