//
//  Info.h
//  LivsmedelsApp
//
//  Created by Anders Zetterström on 2015-02-19.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Info : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *itemTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *energyTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *proteinTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *carbohydratesTextLabel;
@property (nonatomic) NSArray *itemNumberArray;
@property (weak, nonatomic) IBOutlet UIImageView *itemImage;
@property (nonatomic) NSString *itemNumber;
@property (nonatomic) int numberOfStars;


@end
