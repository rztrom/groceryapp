//
//  TableViewController.m
//  LivsmedelsApp
//
//  Created by Anders Zetterström on 2015-02-19.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

#import "TableViewController.h"
#import "ViewController.h"
#import "Info.h"
#import "SearchCell.h"

@interface TableViewController ()
@property (nonatomic) NSArray *searchResult;
@property (nonatomic) NSArray *itemArray;
@property (nonatomic) NSString *itemNumber;
@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // kollar om hur lång listan ska vara och retunerar den
    if(tableView == self.tableView){
        return self.fullList.count;
        
    }else {
        return self.searchResult.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    // tillfällig array
    NSArray *array;
    // variabel för att kunna sätta vilken cell som ska användas
    NSString *cellId= @"";
    //sätter vilken data och cell som ska visas i tabellen
    if(tableView == self.tableView){
        array = self.fullList;
        cellId = @"MyCell";
    }else {
        array = self.searchResult;
        cellId=@"ResultCell";
    }
    
     SearchCell *cell = [self.tableView
                         dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    //sätter texten i cellen
    cell.itemCellLabel.text = array[indexPath.row][@"name"];
    //lägger till
    cell.number = array[indexPath.row][@"number"];
    
    return cell;
}


-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSPredicate *predicate =
    [NSPredicate predicateWithFormat:@"name contains[c] %@",searchText];
    //kollar om det man söker på stämmer med name i fullList och lägger det i searchResult
    self.searchResult = [self.fullList filteredArrayUsingPredicate:predicate];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    Info *info = [segue destinationViewController];
    
    SearchCell *cell = sender;
    // sätter texten från cellen till titeln på info-vyn
    info.title= cell.textLabel.text;
    
    //gör en ny sökning med nr på den vara du klickat på i tableviewn
    NSString *searchString =[NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", cell.number];

    //lägger till adressen/sökningen i en url
    NSURL *url = [NSURL URLWithString:searchString];

    //skicka in url i ett request objekt
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    //skapa en session
    NSURLSession *session = [NSURLSession sharedSession];
    // skapa datatask
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        ///kollar om vi fått in fel (om nätverk eller server är fel)
        NSLog(@"completed! Data: %@, Error: %@", data, error);
        // skapar upp parsingError (sätts
        NSError *parsingError= nil;
        // logga error
        if(error){
            NSLog(@"Error in response :%@",  error);
            return ;
        }
        // ta emot data i en array
        NSDictionary *temp= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parsingError];

        // om laddningen av data gick ok
        if(!parsingError){
            //går till main queue
            dispatch_async(dispatch_get_main_queue(),^{
                
                if(temp.count>0){
                    // till en sträng
                    info.itemNumber= [NSString stringWithFormat:@"%@",cell.number];
                    // loopar igenom arrayen med bilder
                    for(int i=0; i<info.itemNumberArray.count; i++){
                        //kollar om det finns en bild som heter som nr:et i reqesten
                        if ([[info.itemNumberArray objectAtIndex:i] isEqualToString:info.itemNumber]) {
                            //sätter bilden 
                            info.itemImage.image = [UIImage imageNamed:info.itemNumber];
                        }
                    }
                    //sätter antalet stjärnor till 5 (max)
                    info.numberOfStars=5;
                    
                    //minskar med en stjärna om alkoholen är för hög
                    if([temp[@"nutrientValues"][@"alcohol"] doubleValue]>3){
                        info.numberOfStars-=1;
                    }
                    // minskar med en stjärna om fetthalten är för hög
                    if([temp[@"nutrientValues"][@"fat"] doubleValue]>8){
                        info.numberOfStars-=1;
                    }
                    // minskar med en stjärna om det är för mycket salt
                    if([temp[@"nutrientValues"][@"salt"] doubleValue]>0.6){
                        info.numberOfStars-=1;
                    }
                    // minskar med en stjärna om kolesterolen är för hög
                    if([temp[@"nutrientValues"][@"cholesterol"] doubleValue]>95){
                        info.numberOfStars-=1;
                    }
                    // minskar med en stjärna om proteinnivån är för låg
                    if([temp[@"nutrientValues"][@"protein"] doubleValue]<10){
                        info.numberOfStars-=1;
                    }
                    // sätter textlabels på info-sidan
                    info.itemTextLabel.text = temp[@"name"];
                    info.energyTextLabel.text=[NSString stringWithFormat:@"%@ %@", temp[@"nutrientValues"][@"energyKcal"],@"Kcal"];
                    info.proteinTextLabel.text=[NSString stringWithFormat:@"%@ %@", temp[@"nutrientValues"][@"protein"],@"g"];
                    info.fatTextLabel.text=[NSString stringWithFormat:@"%@ %@", temp[@"nutrientValues"][@"fat"],@"g"];
                    info.carbohydratesTextLabel.text=[NSString stringWithFormat:@"%@ %@", temp[@"nutrientValues"][@"carbohydrates"],@"g"];
                }else {
                    
                    info.itemTextLabel.text = @"no topics found";
                }
            });
        } else {
            // om det blev parsing error , skriver ut i loggen
            NSLog(@"Couldnt parse json : %@", parsingError);
        }
        
    }];
    // startar uppgiften
    [task resume];
}


@end
